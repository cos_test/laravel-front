<?php

namespace App\Http\Controllers;

use App\Services\InvoiceService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public $invoiceService;

    public function __construct(InvoiceService $invoiceService){
        $this->invoiceService = $invoiceService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $customers = json_decode($this->invoiceService->getCustomers())->data;
        foreach ($customers as $key => $customer){
            $customerSelect[$customer->id] = $customer->name;
        }
        $data = json_decode($this->invoiceService->getInvoices())->data;
        return view('invoices.index',['invoices'=>$data, 'customers'=> $customerSelect]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $customers = json_decode($this->invoiceService->getCustomers())->data;
        foreach ($customers as $key => $customer){
            $customerSelect[$customer->id] = $customer->name;
        }
        return view('invoices.create', ['customers'=>$customerSelect]);
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(Request $request)
    {
        $data = json_decode($this->invoiceService->SetInvoice($request->all()))->data;
        return redirect('invoices');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
