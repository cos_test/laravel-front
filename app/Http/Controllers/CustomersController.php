<?php

namespace App\Http\Controllers;

use App\Services\CustomerService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CustomersController extends Controller
{
    public $customerService;

    public function __construct(CustomerService $customerService){
        $this->customerService = $customerService;
    }

    public function index()
    {
        $data = json_decode($this->customerService->getCustomers())->data;
        return view('customers.index',['customers'=>$data]);
    }

}
