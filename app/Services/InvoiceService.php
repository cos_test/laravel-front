<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;

class InvoiceService
{
    use ConsumesExternalService;

    /**
     * The base uri to be used to consume the authors service
     * @var string
     */
    public $baseUri;

    public function __construct()
    {
        $this->baseUri = 'localhost:8001';
    }

    public function getInvoices()
    {
        return $this->performRequest('GET', '/invoices');
    }

    public function getCustomers()
    {
        return $this->performRequest('GET', '/customers');
    }

    public function setInvoice($data)
    {
        return $this->performRequest('POST', '/invoices', $data);
    }


}
