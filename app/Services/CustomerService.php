<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;

class CustomerService
{
    use ConsumesExternalService;

    /**
     * The base uri to be used to consume the authors service
     * @var string
     */
    public $baseUri;

    public function __construct()
    {
        $this->baseUri = 'localhost:8001';
    }

    public function getCustomers()
    {
        return $this->performRequest('GET', '/customers');
    }


}
