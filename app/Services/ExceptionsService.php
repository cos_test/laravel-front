<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;

class ExceptionsService
{
    use ConsumesExternalService;

    /**
     * The base uri to be used to consume the authors service
     * @var string
     */
    public $baseUri;

    public function __construct()
    {
        $this->baseUri = 'localhost:8002';
    }

    public function getExceptions()
    {
        return $this->performRequest('GET', '/exceptions');
    }
}
