@extends('dashboard')
@section('section')
<div class="row">
    <div class="col-xl-12">
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-chart-area me-1"></i>
                Area Chart Example
            </div>
            <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas>
                {!!Form::open(['route'=>'invoices.store','method'=>'POST'])!!}
                @include('invoices.fields')
                <div class="form-group">
                    {!!Form::submit('Create', ['class'=>'btn btn-success pull-right'])!!}
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>
@endsection
