@extends('dashboard')
@section('section')
<div class="container-fluid px-4">
    <h1 class="mt-4">Dashboard</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            DataTable Example
        </div>
        <div class="card-body">
            <table id="datatablesSimple">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Customer</th>
                    <th>Number</th>
                    <th>Value</th>
                    <th>Company</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Id</th>
                    <th>Customer</th>
                    <th>Number</th>
                    <th>Value</th>
                    <th>Company</th>
                </tr>
                </tfoot>
                <tbody>
                @foreach ($invoices as $invoice)
                    <tr>
                        <td>{{$invoice->id}}</td>
                        <td>{{$customers[$invoice->customer_id]}}</td>
                        <td>{{$invoice->number}}</td>
                        <td>{{$invoice->value}}</td>
                        <td>{{$invoice->company}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
