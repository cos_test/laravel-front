<div class="form-group">
    {!!Form::label('Customer')!!}
    {!!Form::select('customer_id', $customers , null, ['class'=>'form-control' , 'placeholder'=>'Please enter position'])!!}
</div>
<div class="form-group">
    {!!Form::label('Number')!!}
    {!!Form::text('number', null , ['class'=>'form-control' , 'placeholder'=>'Please enter number'])!!}
</div>
<div class="form-group">
    {!!Form::label('Products')!!}
    {!!Form::text('products', null , ['class'=>'form-control' , 'placeholder'=>'Please enter products'])!!}
</div>
<div class="form-group">
    {!!Form::label('Company')!!}
    {!!Form::text('company', null , ['class'=>'form-control' , 'placeholder'=>'Please enter company'])!!}
</div>
