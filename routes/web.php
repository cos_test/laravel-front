<?php

use App\Http\Controllers\CustomersController;
use App\Http\Controllers\ExceptionsController;
use App\Http\Controllers\InvoiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('invoices', InvoiceController::class);
Route::resource('exceptions', ExceptionsController::class);
Route::resource('customers', CustomersController::class);
